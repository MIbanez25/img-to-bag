import os
import argparse
import numpy as np
import cv2
import rosbag
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from multiprocessing.pool import ThreadPool as Pool

nb_cores = 6

def get_frame(topic, msg,t,count,output_dir):
    np_arr = np.fromstring(msg.data, np.uint8)
    image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
    cv2.imwrite(os.path.join(output_dir, "frame%06i.jpg" % count),cv2.rotate(image_np, cv2.ROTATE_90_CLOCKWISE),[cv2.IMWRITE_JPEG_QUALITY, 100])
    print ("Wrote image %i" % count)

def main():
    """Extract a folder of images from a rosbag.
    """
    p = Pool(nb_cores)

    parser = argparse.ArgumentParser(description="Extract images from a ROS bag.")
    parser.add_argument("bag_file", help="Input ROS bag.")
    parser.add_argument("image_topic", help="Image topic.")

    # Creates the directory for the images based on image topic
    args = parser.parse_args()
    num = args.bag_file.rfind('/')
    num1 = args.image_topic[1:].find('/')
    output_dir = (args.bag_file[:num+1] + args.image_topic[:num1+1])

    if (os.path.exists(output_dir)):
        print("Directory already exists")
        exit
    else:
        print("Creating Directory: ", output_dir)
        os.mkdir(output_dir)

    print ("Extract images from %s on topic %s into %s" % (args.bag_file,args.image_topic, output_dir))

    bag = rosbag.Bag(args.bag_file, "r")
    count = 0
    for topic, msg, t in bag.read_messages(topics=[args.image_topic]):
        p.apply_async(get_frame, (topic, msg, t, count,output_dir))
        count+=1
    p.close()
    p.join()
    bag.close()
    return

if __name__ == '__main__':
    main()